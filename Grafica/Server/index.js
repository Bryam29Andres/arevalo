//Cheking correct usage of application => node application.js
if(process.argv[2] == undefined){
    global.console.log('Usage: mode application.js <COM#>');
    process.exit(1);
}
let portCom = process.argv[2];

const express = require('express');
const SocketIO = require('socket.io');
const http = require('http');

const app = express();
const server = http.createServer(app);
const io = SocketIO.listen(server);

io.on('connection', function(socket){
    console.log('a new socket connected');
})

app.get('/',(req, res, next)=>{
    res.sendFile(__dirname + 'index.html');
});

/*Serial port */
const SerialPort = require('serialport');
const Readline = SerialPort.parsers.Readline;
const parser = new Readline();

const mySerial = new SerialPort(portCom, {
    baudRate: 9600
});

mySerial.on('open', function(){
    console.log('Opened Serial port');
});

mySerial.on('data', function(data){
    console.log(data);
    io.emit('arduino:data', {
        value: data.toString()
    });
});

mySerial.on('error', (err) => console.log(err));

server.listen(3000, () => {
    console.log('server on port', 3000);
});