#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Max72xxPanel.h>

#define CLK_PIN   13
#define DATA_PIN  11
#define CS_PIN    10

#define reset 8

const int numberOfHorizontalDisplays = 8;
const int numberOfVerticalDisplays = 1;

Max72xxPanel matrix = Max72xxPanel(CS_PIN, numberOfHorizontalDisplays, numberOfVerticalDisplays);
const int wait = 20; // Velocidad a la que realiza el scroll
const int spacer = 1;
const int width = 5 + spacer; // Ancho de la fuente a 5 pixeles

long randNumber; //numero randomico
int step_counter = 0; //contador
int button_values[] = {1005, 906, 835, 745, 566}; //valores de entrada de los pulsantes
int btn_tol = 10; //total de botones
int analogValue = 0; //valor de entrada analogica
int pin_p1 = A1; //pin analogico donde se conectan los pulsantes
int leds_cnt = 5;
int p1_leds[5] = {2,3,4,5,6}; //pines donde van los leds
int p1_score = 0; //contador de puntuacion
int action_speed = 2000;
int action_speed_min = 250;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

  randomSeed(analogRead(A7)); //puerto para el ramdom
  pinMode(pin_p1, INPUT);
  pinMode(reset, INPUT);

  for (int i = 0; leds_cnt > i; i++) {
    pinMode(p1_leds[i], OUTPUT);
  }

    matrix.setIntensity(1); // Ajustar el brillo entre 0 y 15
    matrix.setPosition(0, 0, 0); // El primer display esta en <0, 0>
    matrix.setPosition(1, 1, 0); // El segundo display esta en <1, 0>
    matrix.setPosition(2, 2, 0); // El tercer display esta en <2, 0>
    matrix.setPosition(3, 3, 0); // El cuarto display esta en <3, 0>
    matrix.setPosition(4, 4, 0); // El quinto display esta en <4, 0>
    matrix.setPosition(5, 5, 0); // El sexto display esta en <5, 0>
    matrix.setPosition(6, 6, 0); // El séptimo display esta en <6, 0>
    matrix.setPosition(7, 7, 0); // El octavo display esta en <7, 0>
    matrix.setPosition(8, 8, 0); // El noveno display esta en <8, 0>
    matrix.setRotation(0, 1);    // Posición del display
    matrix.setRotation(1, 1);    // Posición del display
    matrix.setRotation(2, 1);    // Posición del display
    matrix.setRotation(3, 1);    // Posición del display
    matrix.setRotation(4, 1);    // Posición del display
    matrix.setRotation(5, 1);    // Posición del display
    matrix.setRotation(6, 1);    // Posición del display
    matrix.setRotation(7, 1);    // Posición del display
    matrix.setRotation(8, 1);    // Posición del display
  
  initBienvenida();
}

void loop() {
  // put your main code here, to run repeatedly:
    if(p1_score < 20) {
    step_counter++;
    boolean step_action = false;

    if (step_counter > action_speed) {
      step_counter = 0;
      step_action = true;  
      action_speed = action_speed - round(action_speed/50);
      if (action_speed < action_speed_min) {
        action_speed = action_speed_min;
      }
      Serial.println(action_speed);
      delay(200);
    }
  
    if (step_action) {
      int pin_light = random(0,5);
      Serial.println(pin_light);
      digitalWrite(p1_leds[pin_light], HIGH);
      delay(100);
    }
    
    analogValue = analogRead(pin_p1);
    for (int i = 0; leds_cnt > i; i++) {
      if ( analogValue < button_values[i] + btn_tol and analogValue > button_values[i] - btn_tol ){
        if(digitalRead(p1_leds[i]) == HIGH){
          digitalWrite(p1_leds[i], LOW);
          delay(50);
          p1_score++;
        }
      }
    }
  }else{
    String cadena = "GAME OVER - RESTART ";

    for(int i = 0; i < width*cadena.length()+matrix.width()-1-spacer; i++){
      matrix.fillScreen(LOW);
      int letter = i / width;
      int x = (matrix.width() - 1) - i % width;
      int y = (matrix.height() - 8) / 2; // Centrar el texto
      while(x + width - spacer >= 0 && letter >= 0){
        if(letter < cadena.length()){
          matrix.drawChar(x, y, cadena[letter], HIGH, LOW, 1);
        }
        letter--;
        x -= width;
      }
      matrix.write(); // Muestra los caracteres
      delay(wait);
    }
    
    digitalWrite(2, HIGH);
    digitalWrite(3, HIGH);
    digitalWrite(4, HIGH);
    digitalWrite(5, HIGH);
    digitalWrite(6, HIGH);
    delay(200);
    digitalWrite(2,LOW);
    digitalWrite(3, LOW);
    digitalWrite(4, LOW);
    digitalWrite(5, LOW);
    digitalWrite(6, LOW);
    delay(200);

    //reiniciar();
    //p1_score=0;
    //initBienvenida();

    //************* Leer boton RESET ***********
    int stateR = digitalRead(reset);
    Serial.println(stateR);
    delay(200);
    if(stateR==1){
      p1_score=0;
      initBienvenida();
    } 
  }
}

void initBienvenida(){
  boolean ver = false;
  while(!ver){
    String cadena = "BIENVENIDOS - PROYECTO PUSH-LED ";
    long int time = millis();
    for(int i = 0; i < width*cadena.length()+matrix.width()-1-spacer; i++){
      matrix.fillScreen(LOW);
      int letter = i / width;
      int x = (matrix.width() - 1) - i % width;
      int y = (matrix.height() - 8) / 2; // Centrar el texto
      while(x + width - spacer >= 0 && letter >= 0){
         if(letter < cadena.length()){
             matrix.drawChar(x, y, cadena[letter], HIGH, LOW, 1);
         
            //int j=0;
            //prenderLeds(j);
            //j++;
         }
         letter--;
         x -= width;
      }
      matrix.write(); // Muestra los caracteres
      delay(wait);
    }
    ver=true;
  }
}
/*
reiniciar(){
  int stateR = 0;
  while(stateR){
    stateR = digitalRead(reset);
    p1_score=0;
    initBienvenida();
    
  }
}
*/
void prenderLeds(int i){
  if(i==0){
      digitalWrite(2, HIGH);
      digitalWrite(3, LOW);
      digitalWrite(4, LOW);
      digitalWrite(5, LOW);
      digitalWrite(6, LOW);
   }
  
   if(i==1){
      digitalWrite(2, LOW);
      digitalWrite(3, HIGH);
      digitalWrite(4, LOW);
      digitalWrite(5, LOW);
      digitalWrite(6, LOW);
   }
  
   if(i==2){
      digitalWrite(2, LOW);
      digitalWrite(3, LOW);
      digitalWrite(4, LOW);
      digitalWrite(5, LOW);
      digitalWrite(6, HIGH);          
   }
  
   if(i==3){
      digitalWrite(2, LOW);
      digitalWrite(3, LOW);
      digitalWrite(4, LOW);
      digitalWrite(5, HIGH);
      digitalWrite(6, LOW);
   }
  
   if(i==4){
      digitalWrite(2, LOW);
      digitalWrite(3, LOW);
      digitalWrite(4, HIGH);
      digitalWrite(5, LOW);
      digitalWrite(6, LOW);
   }
  
   if(i>=5){
      digitalWrite(2, LOW);
      digitalWrite(3, LOW);
      digitalWrite(4, LOW);
      digitalWrite(5, LOW);
      digitalWrite(6, LOW);          
   }
}
